package com.bnssolutions.snippet.struts_tutorial;


import com.opensymphony.xwork2.ActionSupport;
import com.bnssolutions.snippet.struts_tutorial.Person;


public class RegisterPersonAction extends ActionSupport {
    private static final long serialVersionUID = 1;
    private Person            personBean;

    @Override
    public String execute() {
        return SUCCESS;
    }

    public Person getPersonBean() {
        return personBean;
    }

    public void setPersonBean(Person person) {
        this.personBean = person;
    }
}
