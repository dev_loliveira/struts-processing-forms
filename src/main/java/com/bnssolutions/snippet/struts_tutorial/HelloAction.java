package com.bnssolutions.snippet.struts_tutorial;


import com.bnssolutions.snippet.struts_tutorial.MessageStore;
import com.opensymphony.xwork2.ActionSupport;

 
public class HelloAction extends ActionSupport {
    private static int        count = 0;
    private static final long serialVersionUID = 1L;
    private MessageStore      messageStore;
    private String            userName;

    public String execute() throws Exception {
        count++;
        messageStore = new MessageStore("Olá");

        if(userName != null)
            messageStore.setMessage(messageStore.getMessage() + ", " + userName);

        return SUCCESS;
    }
 
    public MessageStore getMessageStore() {
        return messageStore;
    }
 
    public void setMessageStore(MessageStore messageStore) {
        this.messageStore = messageStore;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }
}
