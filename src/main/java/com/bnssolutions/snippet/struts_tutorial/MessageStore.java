package com.bnssolutions.snippet.struts_tutorial;


public class MessageStore {
    private String message;

    public MessageStore(String message) {
        setMessage(message);
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String toString() {
        return message + " (toString)";
    }
}
