<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Basic Struts 2 Application - Welcome</title>
</head>
<body>
    <h1>Welcome To Struts 2!</h1>

    <s:url action="hello" var="helloLink">
    <s:param name="userName">Dummy Name</s:param>
    </s:url>
    <p><a href="${helloLink}">Hello dummy user</a></p>
    <p><a href="register.jsp">Registro</a></p>

</body>
</html>
