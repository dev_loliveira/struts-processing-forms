<!-- 
Struts tags reference
https://struts.apache.org/docs/ui-tag-reference.html
-->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
      <title>Hello World com MVC!</title>
    </head>

    <br/>Total requests to this page: <s:property value="count" />
    <br/>toString example: <s:property value="messageStore" />
 
    <body>
      <h2><s:property value="messageStore.message" /></h2>

      <s:form action="hello" method="POST">
          <s:textfield name="userName" label="Enter your name" />
          <s:submit value="Send" />
      </s:form>
    </body>

</html>
